/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * systemd-gtk-module.c
 * Copyright (C) 2021 Red Hat, Inc. All rights reserved.
 * 
 * Authors:
 *   Akira TAGOH  <tagoh@redhat.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include "imsettings-info.h"
#include "imsettings-utils.h"

void      module_switch_im(IMSettingsInfo *info);
gchar    *module_dump_im  (void);

/*< private >*/

/*< public >*/
void
module_switch_im(IMSettingsInfo *info)
{
	const gchar *gtkimm = imsettings_info_get_gtkimm(info);
	const gchar *xim = imsettings_info_get_xim(info);
	gchar *envd = g_build_filename(g_get_user_config_dir(), "environment.d", NULL);
	gchar *filename = g_build_filename(envd, "imsettings-gtk.conf", NULL);
	gchar *val = NULL;
	gchar *s = NULL;
	GError *error = NULL;

	if (!gtkimm || gtkimm[0] == 0) {
		g_warning("Invalid gtk immodule in: %s",
			  imsettings_info_get_filename(info));
		goto finalize;
	}
	if (!xim || xim[0] == 0) {
		g_warning("Invalid xim in: %s",
			  imsettings_info_get_filename(info));
		goto finalize;
	}
#ifdef ENABLE_FALLBACK_IM
	val = g_strdup_printf("%s:xim", gtkimm);
#else
	val = g_strdup(gtkimm);
#endif
	s = g_strdup_printf("GTK_IM_MODULE=%s\n"
			    "XMODIFIERS=@im=%s\n",
			    val, xim);
	if (g_mkdir_with_parents(envd, 0700) != 0) {
		int save_errno = errno;

		g_warning("Failed to create the config dir: %s",
			  g_strerror(save_errno));
		goto finalize;
	}
	if (!g_file_set_contents (filename, s, -1, &error)) {
		if (error) {
			g_warning("Unable to write config file: %s", error->message);
		} else {
			g_warning("Unable to write config file due to the unknown error");
		}
		goto finalize;
	}
	g_log(G_LOG_DOMAIN, G_LOG_LEVEL_INFO,
	      "Setting up %s as gtk+ immodule",
	      val);
	g_log(G_LOG_DOMAIN, G_LOG_LEVEL_INFO,
	      "Setting up %s as XMODIFIERS",
	      xim);
	g_log(G_LOG_DOMAIN, G_LOG_LEVEL_INFO,
	      "Reboot may require to apply.");
  finalize:
	g_free(s);
	g_free(val);
	g_free(envd);
	g_free(filename);
	if (error) {
		g_error_free(error);
	}
}

gchar *
module_dump_im(void)
{
	gchar *retval = NULL, *config;
	gchar *filename = g_build_filename(g_get_user_config_dir(), "environment.d", "imsettings-gtk.conf", NULL);
	gsize len;
	GError *error = NULL;

	if (g_file_get_contents(filename, &config, &len, &error)) {
		gchar *p, *s;

		if ((p = g_strstr_len(config, -1, "GTK_IM_MODULE=")) != NULL) {
			p += 14;
			for (s = p; s != 0 && !g_ascii_isspace(*s); s++);
			*s = 0;
			retval = g_strdup(p);
		}
		g_free(config);
	}
	g_free(filename);

	return retval;
}
