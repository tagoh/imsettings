# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the imsettings package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: imsettings 1.8.6\n"
"Report-Msgid-Bugs-To: https://bitbucket.org/tagoh/imsettings/issues/new\n"
"POT-Creation-Date: 2023-09-20 22:31+0900\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: backends/xim/loopback.c:223
msgid "Synchronous"
msgstr ""

#: backends/xim/loopback.c:224
msgid "Request to send a key event synchronously"
msgstr ""

#: backends/xim/main.c:155
msgid "D-Bus address to use"
msgstr ""

#: backends/xim/main.c:155
msgid "ADDRESS"
msgstr ""

#: backends/xim/main.c:156
msgid "X display to use"
msgstr ""

#: backends/xim/main.c:156
msgid "DISPLAY"
msgstr ""

#: backends/xim/main.c:157
msgid "Replace the running XIM server with new instance."
msgstr ""

#: backends/xim/main.c:158
msgid "Output the debugging logs"
msgstr ""

#: backends/xim/main.c:159
msgid "XIM server connects to, for debugging purpose only"
msgstr ""

#: backends/xim/main.c:159
msgid "XIM"
msgstr ""

#: backends/xim/main.c:186 imsettings-daemon/main.c:139
#: utils/imsettings-check.c:352 utils/imsettings-info.c:69
#: utils/imsettings-reload.c:68 utils/imsettings-switch.c:82
msgid "Unknown error in parsing the command lines."
msgstr ""

#: backends/xim/proxy.c:2958
msgid "XIM server name"
msgstr ""

#: backends/xim/proxy.c:2959
msgid "The name of XIM server for the connection"
msgstr ""

#: backends/xim/proxy.c:2964
msgid "Signals for Protocol class"
msgstr ""

#: backends/xim/proxy.c:2965
msgid "A structure of signals for Protocol class"
msgstr ""

#: imsettings/imsettings-client.c:170
msgid "Locale"
msgstr ""

#: imsettings/imsettings-client.c:171
msgid "Locale to get the imsettings information"
msgstr ""

#: imsettings/imsettings-client.c:176
msgid "Desktop"
msgstr ""

#: imsettings/imsettings-client.c:177
msgid "Current desktop"
msgstr ""

#: imsettings-daemon/imsettings-module.c:129
msgid "Name"
msgstr ""

#: imsettings-daemon/imsettings-module.c:130
msgid "A module name for imsettings backend"
msgstr ""

#: imsettings-daemon/imsettings-proc.c:193
msgid "Unable to keep Input Method running"
msgstr ""

#: imsettings-daemon/imsettings-proc.c:203
#, c-format
msgid ""
"Giving up to bring the process up because %s Input Method process for %s "
"rapidly died many times. See $XDG_CACHE_HOME/imsettings/log for more details."
msgstr ""

#: imsettings-daemon/imsettings-proc.c:304
#, c-format
msgid "[BUG] %s process is still running [pid: %d]\n"
msgstr ""

#: imsettings-daemon/imsettings-proc.c:450
#, c-format
msgid "Couldn't send a signal to the %s process successfully."
msgstr ""

#: imsettings-daemon/imsettings-proc.c:579
msgid "IMSettingsInfo"
msgstr ""

#: imsettings-daemon/imsettings-proc.c:580
msgid "A GObject to be a IMSettingsInfo"
msgstr ""

#: imsettings-daemon/imsettings-proc.c:585
msgid "Desktop name"
msgstr ""

#: imsettings-daemon/imsettings-proc.c:586
msgid "A desktop name for client"
msgstr ""

#: imsettings-daemon/imsettings-server.c:415
msgid "DBus connection"
msgstr ""

#: imsettings-daemon/imsettings-server.c:416
msgid "A GObject to be a DBus connection"
msgstr ""

#: imsettings-daemon/imsettings-server.c:421
msgid "Logging"
msgstr ""

#: imsettings-daemon/imsettings-server.c:422
msgid "A boolean value whether the logging facility is enabled or not."
msgstr ""

#: imsettings-daemon/imsettings-server.c:427
#: imsettings-daemon/imsettings-server.c:428
msgid "Home directory"
msgstr ""

#: imsettings-daemon/imsettings-server.c:433
#: imsettings-daemon/imsettings-server.c:434
msgid "xinputrc directory"
msgstr ""

#: imsettings-daemon/imsettings-server.c:439
#: imsettings-daemon/imsettings-server.c:440
msgid "xinput directory"
msgstr ""

#: imsettings-daemon/imsettings-server.c:445
msgid "module directory"
msgstr ""

#: imsettings-daemon/imsettings-server.c:446
msgid "IMSettings module directory"
msgstr ""

#: imsettings-daemon/imsettings-server.c:800
#, c-format
msgid "No such input method on your system: %s"
msgstr ""

#: imsettings-daemon/imsettings-server.c:809
#: imsettings-daemon/imsettings-server.c:951
#: imsettings-daemon/imsettings-server.c:992
#: imsettings-daemon/imsettings-server.c:1208
#: imsettings-daemon/imsettings-server.c:1238
msgid "Out of memory"
msgstr ""

#: imsettings-daemon/imsettings-server.c:817
msgid "Current desktop isn't targeted by IMSettings."
msgstr ""

#: imsettings-daemon/imsettings-server.c:873
#, c-format
msgid "Failed to revert the backup file: %s"
msgstr ""

#: imsettings-daemon/imsettings-server.c:886
#, c-format
msgid "Failed to create a backup file: %s"
msgstr ""

#: imsettings-daemon/imsettings-server.c:896
#, c-format
msgid "Failed to remove an user xinputrc file: %s"
msgstr ""

#: imsettings-daemon/imsettings-server.c:907
#, c-format
msgid "Failed to create a symlink: %s"
msgstr ""

#: imsettings-daemon/imsettings-server.c:944
msgid "No system-wide xinputrc available"
msgstr ""

#: imsettings-daemon/imsettings-server.c:984
msgid "No user xinputrc nor system-wide xinputrc available"
msgstr ""

#: imsettings-daemon/imsettings-server.c:1200
#: imsettings-daemon/imsettings-server.c:1230
#, c-format
msgid "No such input method: %s"
msgstr ""

#: imsettings-daemon/main.c:111
msgid "Replace the instance of the imsettings daemon."
msgstr ""

#: imsettings-daemon/main.c:112
msgid "Set the system-wide xinputrc directory (for debugging purpose)"
msgstr ""

#: imsettings-daemon/main.c:112 imsettings-daemon/main.c:113
#: imsettings-daemon/main.c:114 imsettings-daemon/main.c:115
msgid "DIR"
msgstr ""

#: imsettings-daemon/main.c:113
msgid "Set the IM configuration directory (for debugging purpose)"
msgstr ""

#: imsettings-daemon/main.c:114
msgid "Set a home directory (for debugging purpose)"
msgstr ""

#: imsettings-daemon/main.c:115
msgid "Set the imsettings module directory (for debugging purpose)"
msgstr ""

#: imsettings-daemon/main.c:116
msgid "Do not create a log file."
msgstr ""

#: utils/imsettings-check.c:132
msgid "Unable to open X display"
msgstr ""

#: utils/imsettings-check.c:139
msgid "XSETTINGS manager isn't running"
msgstr ""

#: utils/imsettings-check.c:170 utils/imsettings-check.c:223
#: utils/imsettings-check.c:287
msgid "Unable to create a client instance."
msgstr ""

#: utils/imsettings-check.c:178 utils/imsettings-check.c:231
#: utils/imsettings-check.c:295
msgid "imsettings version mismatch"
msgstr ""

#: utils/imsettings-check.c:191 utils/imsettings-check.c:257
msgid "No modules loaded"
msgstr ""

#: utils/imsettings-check.c:249
msgid "Please see .imsettings.log for more details"
msgstr ""

#: utils/imsettings-check.c:325
msgid "Output the detail information for the result"
msgstr ""

#: utils/imsettings-check.c:326
msgid "Check if DBus is running"
msgstr ""

#: utils/imsettings-check.c:327
msgid "Check if SESSION is on line"
msgstr ""

#: utils/imsettings-check.c:328
msgid "Check if any valid modules are loaded"
msgstr ""

#: utils/imsettings-check.c:329
msgid "Check if all of the module settings has consistencies"
msgstr ""

#: utils/imsettings-check.c:330
msgid "Check if XSETTINGS manager is running"
msgstr ""

#: utils/imsettings-check.c:331
msgid "Check if current desktop is supported"
msgstr ""

#: utils/imsettings-info.c:51 utils/imsettings-switch.c:44
msgid "[Input Method name|xinput.conf]"
msgstr ""

#: utils/imsettings-info.c:75 utils/imsettings-list.c:62
#: utils/imsettings-reload.c:75 utils/imsettings-switch.c:88
msgid "IMSettings is disabled on the system.\n"
msgstr ""

#: utils/imsettings-info.c:81 utils/imsettings-list.c:68
#: utils/imsettings-reload.c:81 utils/imsettings-switch.c:94
msgid "Unable to create a client instance.\n"
msgstr ""

#: utils/imsettings-info.c:88 utils/imsettings-list.c:75
#: utils/imsettings-switch.c:126
msgid ""
"Currently a different version of imsettings is running.\n"
"Running \"imsettings-reload\" may help but it will restart the Input Method\n"
msgstr ""

#: utils/imsettings-info.c:106
#, c-format
msgid "Unable to obtain an Input Method Information: %s\n"
msgstr ""

#: utils/imsettings-reload.c:45
msgid "Force reloading imsettings-daemon (deprecated)."
msgstr ""

#: utils/imsettings-reload.c:113
msgid "Reloaded.\n"
msgstr ""

#: utils/imsettings-start.desktop:35 utils/imsettings-start.desktop:66
msgid "Input Method starter"
msgstr ""

#: utils/imsettings-switch.c:47
msgid "Force restarting the IM process regardless of any errors."
msgstr ""

#: utils/imsettings-switch.c:49
msgid "Use the given string as current desktop name."
msgstr ""

#: utils/imsettings-switch.c:51
msgid "Do not update the user xinputrc."
msgstr ""

#: utils/imsettings-switch.c:53
msgid "Shut up the extra messages."
msgstr ""

#: utils/imsettings-switch.c:55
msgid "Restart input method"
msgstr ""

#: utils/imsettings-switch.c:57
msgid "Read xinputrc to determine the input method"
msgstr ""

#: utils/imsettings-switch.c:115
msgid "Unable to detect current desktop."
msgstr ""

#: utils/imsettings-switch.c:131
msgid ""
"Current desktop isn't supported by IMSettings. Please follow instructions on "
"your desktop to enable Input Method.\n"
msgstr ""

#: utils/imsettings-switch.c:140
msgid "No backend modules available"
msgstr ""

#: utils/imsettings-switch.c:150
msgid "Unable to communicate with imsettings-daemon"
msgstr ""

#: utils/imsettings-switch.c:171
msgid "No Input Method running to be restarted.\n"
msgstr ""

#: utils/imsettings-switch.c:176 utils/imsettings-switch.c:216
msgid "No actions required.\n"
msgstr ""

#: utils/imsettings-switch.c:188
#, c-format
msgid "Restarted %s\n"
msgstr ""

#: utils/imsettings-switch.c:234
#, c-format
msgid "Switched input method to %s\n"
msgstr ""
