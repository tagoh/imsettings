/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * imsettings-switch.c
 * Copyright (C) 2008-2021 Red Hat, Inc. All rights reserved.
 * 
 * Authors:
 *   Akira TAGOH  <tagoh@redhat.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301  USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <locale.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib/gi18n.h>
#include "imsettings.h"
#include "imsettings-client.h"
#include "imsettings-utils.h"

int
main(int    argc,
     char **argv)
{
	IMSettingsClient *client = NULL;
	IMSettingsInfo *info = NULL;
	gchar *locale, *module = NULL, *arg_desktop = NULL;
	gboolean arg_force = FALSE, arg_no_update = FALSE, arg_restart = FALSE, arg_xinputrc = FALSE, arg_quiet = FALSE;
	GOptionContext *ctx = g_option_context_new(_("[Input Method name|xinput.conf]"));
	GOptionEntry entries[] = {
		/* For translators: this is a translation for the command-line option. */
		{"force", 'f', 0, G_OPTION_ARG_NONE, &arg_force, N_("Force restarting the IM process regardless of any errors."), NULL},
		/* For translators: this is a translation for the command-line option. */
		{"desktop", 'd', 0, G_OPTION_ARG_STRING, &arg_desktop, N_("Use the given string as current desktop name."), NULL},
		/* For translators: this is a translation for the command-line option. */
		{"no-update", 'n', 0, G_OPTION_ARG_NONE, &arg_no_update, N_("Do not update the user xinputrc."), NULL},
		/* For translators: this is a translation for the command-line option. */
		{"quiet", 'q', 0, G_OPTION_ARG_NONE, &arg_quiet, N_("Shut up the extra messages."), NULL},
		/* For translators: this is a translation for the command-line option. */
		{"restart", 'r', 0, G_OPTION_ARG_NONE, &arg_restart, N_("Restart input method"), NULL},
		/* For translators: this is a translation for the command-line option. */
		{"read-xinputrc", 'x', 0, G_OPTION_ARG_NONE, &arg_xinputrc, N_("Read xinputrc to determine the input method"), NULL},
		{NULL, 0, 0, 0, NULL, NULL, NULL}
	};
	GError *error = NULL;
	int retval = 0, api_version, retry = 3;
	GVariant *v;
	GVariantIter *iter;
	const gchar *key, *val;

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, IMSETTINGS_LOCALEDIR);
#ifdef HAVE_BIND_TEXTDOMAIN_CODESET
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
#endif /* HAVE_BIND_TEXTDOMAIN_CODESET */
	textdomain (GETTEXT_PACKAGE);
#endif /* ENABLE_NLS */

	setlocale(LC_ALL, "");
	locale = setlocale(LC_CTYPE, NULL);

	g_option_context_add_main_entries(ctx, entries, GETTEXT_PACKAGE);
	if (!g_option_context_parse(ctx, &argc, &argv, &error)) {
		if (error != NULL) {
			g_printerr("%s\n", error->message);
		} else {
			g_warning(_("Unknown error in parsing the command lines."));
		}
		return 1;
	}

	if (!imsettings_is_enabled()) {
		g_printerr(_("IMSettings is disabled on the system.\n"));
		retval = 1;
		goto end;
	}
	client = imsettings_client_new(locale);
	if (!client) {
		g_printerr(_("Unable to create a client instance.\n"));
		retval = 1;
		goto end;
	}
	if (!arg_desktop) {
		gchar *helper_path, *script, *cmd, *out;
		const gchar *pp = g_getenv("IMSETTINGS_HELPER_PATH");
		gint ret;

		if (pp)
			helper_path = g_strdup(pp);
		else
			helper_path = g_strdup(IMSETTINGS_HELPER_PATH);
		script = g_build_filename(helper_path, "imsettings-functions", NULL);
		cmd = g_strdup_printf("bash -c '. %s && echo -n $(get_desktop)'", script);
		g_free(script);
		g_free(helper_path);
		g_spawn_command_line_sync(cmd, &out, NULL, &ret, &error);
		g_free(cmd);
		if (error) {
			g_clear_error(&error);
			g_warning(_("Unable to detect current desktop."));
		} else {
			imsettings_client_set_desktop(client, out);
			g_free(out);
		}
	} else {
		imsettings_client_set_desktop(client, arg_desktop);
	}
	if ((api_version = imsettings_client_get_version(client, NULL, &error)) != IMSETTINGS_SETTINGS_API_VERSION) {
		if (error)
			goto error;
		g_printerr(_("Currently a different version of imsettings is running.\nRunning \"imsettings-reload\" may help but it will restart the Input Method\n"));
		retval = 1;
		goto end;
	}
	if (!imsettings_client_is_supported_desktop(client, NULL, &error)) {
		g_printerr(_("Current desktop isn't supported by IMSettings. Please follow instructions on your desktop to enable Input Method.\n"));
		retval = 1;
		goto end;
	}
	v = imsettings_client_get_module_settings(client, NULL, &error);
	if (error)
		goto error;
	g_variant_get(v, "a{ss}", &iter);
	if (!g_variant_iter_next(iter, "{&s&s}", &key, &val)) {
		g_warning(_("No backend modules available"));
		retval = 1;
		goto end;
	}
	g_variant_iter_free(iter);
	g_variant_unref(v);

  retry:
	if (!imsettings_client_can_x_display_open(client, NULL, &error)) {
		if (retry == 0) {
			g_warning(_("Unable to communicate with imsettings-daemon"));
			retval = 1;
			goto end;
		}
		imsettings_client_reload(client, FALSE, NULL, &error);
		g_object_unref(client);
		sleep(1);
		client = imsettings_client_new(locale);
		retry -= 1;
		goto retry;
	}
	if (arg_restart) {
		const gchar *m, *l;

		info = imsettings_client_get_active_im_info(client, NULL, &error);
		if (error)
			goto error;
		m = imsettings_info_get_short_desc(info);
		l = imsettings_info_get_language(info);
		if (m == NULL || m[0] == 0 ||
		    strcmp(m, IMSETTINGS_NONE_CONF) == 0) {
			g_printerr(_("No Input Method running to be restarted.\n"));
			retval = 1;
			goto end;
		}
		if (!imsettings_client_is_action_needed(client, m, NULL, &error)) {
			g_print(_("No actions required.\n"));
			goto end;
		}
		imsettings_client_switch_im(client, IMSETTINGS_NONE_CONF, FALSE, NULL, &error);
		if (error)
			goto error;
		imsettings_client_set_locale(client, l);
		imsettings_client_switch_im(client, m, FALSE, NULL, &error);
		if (error)
			goto error;

		if (!arg_quiet)
			g_print(_("Restarted %s\n"), m);
		goto end;
	}
	if (argc < 2 && !arg_xinputrc) {
		gchar *help = g_option_context_get_help(ctx, FALSE, NULL);

		g_print("%s", help);
		g_free(help);
		goto end;
	}
	if (arg_xinputrc) {
		const gchar *env = g_getenv("IMSETTINGS_MODULE");

		if (env) {
			GVariant *vv = imsettings_client_get_info_variant(client, env, NULL, NULL);

			if (vv) {
				module = g_strdup(env);
				g_variant_unref(vv);
			}
		}
		if (!module)
			module = imsettings_client_get_user_im(client, NULL, &error);
	} else {
		module = g_strdup(argv[1]);
	}

	if (!imsettings_client_is_action_needed(client, module, NULL, &error)) {
		g_print(_("No actions required.\n"));
		goto end;
	}
	imsettings_client_switch_im(client, module, !arg_no_update, NULL, &error);
	if (error) {
	  error:
		g_printerr("%s\n", error->message);
		if (g_error_matches(error,
				    IMSETTINGS_GERROR,
				    IMSETTINGS_GERROR_NOT_TARGETED_DESKTOP)) {
			g_clear_error(&error);
			imsettings_client_reload(client, api_version < 4, NULL, &error);
		}
		retval = 1;
		g_clear_error(&error);
		goto end;
	}
	if (!arg_quiet)
		g_print(_("Switched input method to %s\n"), module);
  end:
	g_free(module);
	if (info)
		g_object_unref(info);
	if (client)
		g_object_unref(client);
	g_option_context_free(ctx);

	return retval;
}
