#! /bin/sh

topdir="$(dirname $0)/.."

[ ! -x $topdir/data/xinputinfo.sh ] && chmod a+x $topdir/data/xinputinfo.sh

if [ "x$1" = "x-d" ]; then
	shift
	DB="libtool --mode=execute gdb --args "
elif [ "x$1" = "x-t" ]; then
	shift
	DB="libtool --mode=execute valgrind -v --leak-check=full --show-reachable=yes "
fi

IMSETTINGS_HELPER_PATH=$topdir/data PATH=$topdir/backends/xim:$PATH $DB$topdir/imsettings-daemon/imsettings-daemon --replace --no-logfile --moduledir=$topdir/backends/cinnamon-gsettings/.libs:$topdir/backends/gconf/.libs:$topdir/backends/gsettings/.libs:$topdir/backends/lxde/.libs:$topdir/backends/mate-gsettings/.libs:$topdir/backends/mateconf/.libs:$topdir/backends/qt/.libs:$topdir/backends/systemd/.libs:$topdir/backends/xfce/.libs:$topdir/backends/xim/.libs $@
